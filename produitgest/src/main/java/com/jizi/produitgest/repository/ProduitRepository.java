package com.jizi.produitgest.repository;

import com.jizi.produitgest.modele.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProduitRepository extends JpaRepository<Produit, Long> {
}
