package com.jizi.produitgest.modele;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name= "PRODUIT")
//génération auto des methodes d'accès
@Getter
@Setter
@NoArgsConstructor

public class Produit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto-incrémentation de l'ID
    private long id;
    @Column(length = 50)//fixer la taille
    private String nom;
    @Column(length = 150)
    private  String description;
    private Integer prix;
}
